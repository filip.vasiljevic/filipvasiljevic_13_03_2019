#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct Number
{
    int num;
    struct Number * next;
}Number;

Number * add(Number * list, Number * elem);
Number * evenNumbers(Number * dest, Number * origin);
Number * addAscending(Number * list, int num);
bool isEmpty(Number * list);
Number * deleteList(Number * list);
void printAll(Number * list, int ind);

int main()
{
    Number * list = 0, * evenList = 0;
    int command;

    while(true)
    {
        printf("(1) Unos celih brojeva u listu\n");
        printf("(2) Kreiranje liste parnih brojeva iz vec postojece liste\n");
        printf("(3) Izlaz\n");
        scanf("%d", &command);

        switch(command)
        {
        case 1 :
            if(list)
                list = deleteList(list);

            int entry;
            printf("Unesite element liste (0 za prekid unosa)\n");
            scanf("%d", &entry);
            while(entry)
            {
                Number * elem = malloc(sizeof(Number));
                elem->num = entry;
                elem->next = 0;
                list = add(list, elem);
                printf("Unesite element liste (0 za prekid unosa)\n");
                scanf("%d", &entry);
            }
            printAll(list, 1);
            printAll(evenList, 2);
            break;
        case 2 :
            if(list)
            {
                if(evenList)
                    evenList = deleteList(evenList);

                evenList = evenNumbers(evenList, list);
            }
            else
            {
                printf("Ne postoji lista iz koje moze da se izvede lista parnih brojeva\n\n");
                break;
            }
            printAll(list, 1);
            printAll(evenList, 2);
            break;
        case 3 :
            exit(EXIT_SUCCESS);
        default :
            printf("Invalidna komanda\n\n");
        }
    }

    return 0;
}

Number * add(Number * list, Number * elem)
{
    if(list == 0)
    {
        list = elem;
        list->next = 0;
    }
    else
    {
        Number * current;
        for(current = list; current->next; current = current->next);
        current->next = elem;
        elem->next = 0;
    }

    return list;
}

Number * evenNumbers(Number * dest, Number * origin)
{
    for(Number * current = origin; current; current = current->next)
    {
        if(current->num % 2 == 0)
            dest = addAscending(dest, current->num);
    }
    return dest;
}

Number * addAscending(Number * list, int num)
{
    Number * elem = malloc(sizeof(Number));
    elem->num = num;
    elem->next = 0;
    Number * current = list, *prev = 0;

    while(current && current->num < elem->num)
    {
        prev = current;
        current = current->next;
    }

    elem->next = current;

    if(!prev)
        list = elem;
    else
        prev->next = elem;

    return list;
}

bool isEmpty(Number * list)
{
    if(!list)
        return true;

    return false;
}

Number * deleteList(Number * list)
{
    Number * old;
    while (list)
    {
        old=list;
        list=list->next;
        free(old);
    }
    return 0;
}

void printAll(Number * list, int ind)
{
    printf("Svi clanovi %s liste\n\n", ind == 1 ? "" : "parne");
    for(Number * current = list; current; current = current->next)
    {
        printf("%d\n", current->num);
    }
    printf("\n\n");
}
