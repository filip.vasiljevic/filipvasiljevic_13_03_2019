#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Player
{
    int id;
    char name[100];
    int clubID;
    float salary;
    struct Player * next;
    struct Player * prev;
}Player;

typedef struct List
{
    Player * first;
    Player * last;
}List;

List * addDescending(List * list, Player * elem);
List * sackMinMax(List * list);
void printAll(List list);
//void printBackwards(List list);

int main()
{
    List * list;
    list = malloc(sizeof(List));
    list->first = 0;
    list->last = 0;
    int n;
    printf("Koliko fudbalera zelite da unesete\n");
    scanf("%d", &n);

    Player * elem;
    int id, clubID;
    float salary;
    char name[100];
    for(int i = 0; i < n; i++)
    {
        elem = malloc(sizeof(Player));
        printf("Unesite sifru igraca\n");
        scanf("%d", &id);
        fflush(stdin);
        printf("Unesite ime igraca\n");
        scanf("%[^\n]", name);
        printf("Unesite sifru kluba\n");
        scanf("%d", &clubID);
        printf("Unesite platu igraca\n");
        scanf("%f", &salary);
        elem->id = id;
        strcpy(elem->name, name);
        elem->clubID = clubID;
        elem->salary = salary;
        elem->next = 0;
        elem->prev = 0;
        list = addDescending(list, elem);
    }

    printf("Lista pre brisanja\n");
    printAll(*list);
    printf("Lista posle brisanja\n");
    list = sackMinMax(list);
    printAll(*list);

    return 0;
}

List * addDescending(List * list, Player * elem)
{
    Player * current = list->first;

    if(!current)
    {
        list->first = elem;
        list->last = elem;
        elem->prev = 0;
        elem->next = 0;
    }
    else if(elem->salary >= list->first->salary)
    {
        elem->next = list->first;
        elem->next->prev = elem;
        list->first = elem;
    }
    else
    {
        while(current && elem->salary < current->salary)
        {
            current = current->next;
        }
        if(!current)
        {
            list->last->next = elem;
            elem->prev = list->last;
            elem->next = 0;
            list->last = elem;
        }
        else
        {
            elem->prev = current->prev;
            elem->next = current;
            current->prev->next = elem;
            current->prev = elem;
        }
    }

    return list;
}

List * sackMinMax(List * list)
{
    if(list->first == 0)
        return list;

    float max = list->first->salary;
    float min = list->last->salary;

    Player * current = list->first;
    Player * old;
    while(current && current->salary == max)
    {
        old = current;
        current = current->next;
        free(old);
        list->first = current;
        if(current)
            current->prev = 0;
        else
        {
            list->last = 0;
        }
    }

    current = list->last;
    while(current && current->salary == min)
    {
        old = current;
        current = current->prev;
        free(old);
        list->last = current;
        if(current)
            current->next = 0;
        else
        {
            list->first = 0;
        }
    }

    return list;
}

void printAll(List list)
{
    printf("SVI IGRACI\n\n");
    Player * current = list.first;
    for(; current; current = current->next)
    {
        printf("Ime: %s\nSifra: %d\nSifra kluba: %d\nPlata: %f\n\n",
               current->name, current->id, current->clubID, current->salary);
    }
}

//void printBackwards(List list)
//{
//    printf("SVI IGRACI\n\n");
//    Player * current = list.last;
//    for(; current; current = current->prev)
//    {
//        printf("Ime: %s\nSifra: %d\nSifra kluba: %d\nPlata: %f\n\n",
//               current->name, current->id, current->clubID, current->salary);
//    }
//}
