#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct RandNum
{
    int num;
    struct RandNum * next;
}RandNum;

RandNum * add(RandNum * list, RandNum * elem);
void printAll(RandNum * list);
void swapMaxMin(RandNum * list);

int main()
{
    srand(time(NULL));

    RandNum * list = 0;
    int n;
    printf("Unesite broj elemenata liste\n");
    scanf("%d", &n);

    RandNum * temp;
    for(int i = 0; i < n; i++)
    {
        temp = malloc(sizeof(RandNum));
        temp->num = rand();
        temp->next = 0;
        list = add(list, temp);
    }

    printAll(list);
    swapMaxMin(list);
    printAll(list);

    return 0;
}

RandNum * add(RandNum * list, RandNum * elem)
{
    if(list == 0)
    {
        list = elem;
        list->next = 0;
    }
    else
    {
        RandNum * current;
        for(current = list; current->next; current = current->next);
        current->next = elem;
        elem->next = 0;
    }

    return list;
}

void printAll(RandNum * list)
{
    printf("Svi clanovi liste\n\n");
    for(RandNum * current = list; current; current = current->next)
    {
        printf("%d\n", current->num);
    }
    printf("\n\n");
}

void swapMaxMin(RandNum * list)
{
    int max = list->num;
    int min = list->num;

    for(RandNum * current = list; current; current = current->next)
    {
        if(current->num > max)
            max = current->num;
        else if(current->num < min)
            min = current->num;
    }

    for(RandNum * current = list; current; current = current->next)
    {
        if(current->num == max)
            current->num = min;
        else if(current->num == min)
            current->num = max;
    }
}
